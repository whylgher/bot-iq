from iqoptionapi.stable_api import IQ_Option
import time, json, logging, configparser, random
from datetime import datetime, date, timedelta
from dateutil import tz
import sys, license_user, os, getpass
import pandas as pd
logging.disable(level=(logging.DEBUG))
currentDay = datetime.now().day
currentMonth = datetime.now().month
currentYear = datetime.now().year
data_atual = f'{currentDay}:{currentMonth}:{currentYear}'
mes_atual = f'{currentYear}:{currentMonth}'
dia_atual = f'{currentDay}'
#print(mes_atual)
#login
alfabeto = [':','q','1','+','w', 'e','6', 'r', '#','t','2', 'y', 'u','7', 'i', '3','o', 'p', '´', '[', ']','`','¨','{','}',
            '~', '^','$', 'ç','5','!', 'l', 'k','8', 'j', '&','h', '/','g',"'", '@','f','4',')', 'd','0', 's','*', 'a', 'z','x','c','v',
            '9','=','b','%','n','_','m','-',',', '<','>','(','.',';','?']
chave = 7
n = 68
#f_gale = input('Fator do gale, apenas números, recomendado até 2.3\nDigite:')
def decodificar(text):
    text1 = ''
    for letra2 in text:
        indice2 = alfabeto.index(letra2)
        nova_letra2 = alfabeto[(indice2 - chave) % n]
        text1 = text1 + nova_letra2
    return text1
if True:
    if True:
        login_cod = decodificar(str(license_user.azwVaqs_vmAq['uqb`/']))
        data_venc2 = decodificar(str(license_user.VUca_q_aQ['akbvjz9&']))
        mes_venc1 = decodificar(str(license_user.azwVaqs_vmAq['bwab*Q3*']))
    data_dec = data_venc2
    mesvenc = mes_venc1
    login_dec = login_cod[9:]
    verif = login_cod[:9]
    VencEx = mesvenc[-10:]
    MesVencEx = [data_dec[:7], data_dec[7:14], data_dec[14:21], data_dec[21:27]]
    verif2 = ['excedente', 'perspicaz', 'retificar', 'essencial', 'plenitude', 'hegemonia', 'paradigma', 'incidente']
   # print(data_dec)
   # print(MesVencEx)
   # print(MesVencEx)
   # print(VencEx,'\n',data_atual)
   # print(data_atual[:2] ,VencEx[:2], dia_atual)
   # print(verif)
    if verif in verif2:
        #sys.exit()
        pass
    else:
        print('Você esta com problemas em seu contrato, entre em contato com o vendedor.!\n 69 9 9363 5828')
        os.remove("license_user.py")
        time.sleep(2)
        sys.exit()
    while True:
        if data_atual == VencEx:
            print("Esta vencido seu contrato, entre em contato com o vendedor.\n 69 9 9363 5828")
            os.remove("license_user.py")
            time.sleep(2)
            sys.exit()
        if mes_atual in MesVencEx and dia_atual >= VencEx[:2]:
            print("Esta vencido seu contrato, entre em contato com o vendedor.\n 69 9 9363 5828")
            os.remove("license_user.py")
            time.sleep(2)
            sys.exit()
        else:
            print("Verficando o status da sua licença.")
            time.sleep(2)
            break
login = str(login_dec)
print(login)
API = IQ_Option(login, getpass.getpass('Digite sua senha:'))
print("Conectando...")
status, reason = API.connect()
print('\n##### Primeira tentativa #####\nStatus:', status,'\nReason:', reason,"\nEmail:", API.email)
if status == False:
    print('Você não tem altorização de usar este Bot ou a senha está errada.'
          '\nEntre em contato com o desenvolvedor.\n\nWhatsapp: +55 69 9 9363 5828\nFalar com 7Bits')
    time.sleep(3)
    sys.exit()
if reason == "2FA":
    print('\n##### SEGURANÇA DE 2 FATORES HABILITADO #####\nUm sms foi enviado com um código para seu número')
    code_sms = input("Digite o código recebido: ")
    status, reason = API.connect_2fa(code_sms)
    print('\n##### Segunda tentativa #####\nStatus:', status,'\nReason:', reason,"\nEmail:", API.email)
def config_2():
    arquivo = configparser.RawConfigParser()
    arquivo.read('config.txt')
    return {'Conta_real': arquivo.get('GERAL', 'Conta_real')}
config2 = config_2()
if True:
    if config2['Conta_real'] == 'N':
        tipo_conta = 'PRACTICE'  # PRACTICE / REAL
    else:
        tipo_conta = 'REAL'  # PRACTICE / REAL
API.change_balance(tipo_conta)
print("Banca:", API.get_balance(),"\n##############################\n")
def banca():
    return API.get_balance()
def configuracao():
    arquivo = configparser.RawConfigParser()
    arquivo.read('config.txt')

    return {'seguir_ids': arquivo.get('GERAL', 'seguir_ids'), 'stop_win': arquivo.get('GERAL', 'stop_win'),
            'mhi': arquivo.get('GERAL', 'mhi'),'copy': arquivo.get('GERAL', 'copy'),'sinais': arquivo.get('GERAL', 'sinais'),
            'digitais': arquivo.get('GERAL', 'digitais'), 'tendencia': arquivo.get('GERAL', 'tendencia'),
            'stop_loss': arquivo.get('GERAL', 'stop_loss'), 'payout': 0, 'banca_inicial': banca(),
            'filtro_diferenca_sinal': arquivo.get('GERAL', 'filtro_diferenca_sinal'),'fator_gale': arquivo.get('GERAL', 'fator_gale'),
            'martingale': arquivo.get('GERAL', 'martingale'), 'proximo_sinal': arquivo.get('GERAL', 'proximo_sinal'),
            'sorosgale': arquivo.get('GERAL', 'sorosgale'), 'nivel_p_proximo_sinal': arquivo.get('GERAL', 'nivel_p_proximo_sinal'),
            'niveis': arquivo.get('GERAL', 'niveis'), 'filtro_pais': arquivo.get('GERAL', 'filtro_pais'),
            'filtro_top_traders': arquivo.get('GERAL', 'filtro_top_traders'),
            'valor_minimo': arquivo.get('GERAL', 'valor_minimo'), 'paridade': arquivo.get('GERAL', 'paridade'),
            'valor_entrada': arquivo.get('GERAL', 'valor_entrada'), 'timeframe': arquivo.get('GERAL', 'timeframe')}
def timestamp_converter(x, retorno=1):
    hora = datetime.strptime(datetime.utcfromtimestamp(x + 1).strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')
    hora = hora.replace(tzinfo=tz.gettz('GMT'))

    return str(hora.astimezone(tz.gettz('America/Sao Paulo')))[:-6] if retorno == 1 else hora.astimezone(
        tz.gettz('America/Sao Paulo'))
def time_mhi(x, retorno=1):
    hora = datetime.strptime(datetime.utcfromtimestamp(x - 20).strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')
    hora = hora.replace(tzinfo=tz.gettz('GMT'))

    return str(hora.astimezone(tz.gettz('America/Sao Paulo')))[:-6] if retorno == 1 else hora.astimezone(
        tz.gettz('America/Sao Paulo'))
def carregar_sinais():
    arquivo = open('sinais.txt', encoding='UTF-8')
    lista = arquivo.read()
    arquivo.close

    lista = lista.split('\n')

    for index, a in enumerate(lista):
        if a == '':
            del lista[index]

    return lista
def martingale(tipo, valor):
    if tipo == 'simples':
        return valor * float(config['fator_gale'])
def entrada_digital(x,ACTIVES,amount,action,duration):
    _,id=(API.buy_digital_spot(ACTIVES,amount,action,duration))
    if _ == False:
        print('Ativo fechado.')
        sinais(amount,x)
    print(f'\nEntramos com ${round(amount,2)}\n')
    if id !="error":
        while True:
            check,win= API.check_win_digital_v2(id)
            if check==True:
                break
        if win == 0 or None:
            return -1
        if win<0:
            print("Perda de "+str(round(win, 2))+"$")
            return win
        else:
            print("Ganho de "+str(round(win, 2))+"$")
            return win
    else:
        return 'error', 0, False
        #print("Por favor, tente novamente")
def entrada_turbo(x,amount, ACTIVES, action, duration):
    _,id = API.buy(amount, ACTIVES, action, duration)
    if _ == False:
        print('Ativo fechado.')
        sinais(amount,x)
    print(f'\nEntramos com ${round(amount,2)}\n')
    resultado = API.check_win_v3(id)
    if id != "error":
        win = float(resultado[1])
        if win <= 0:
            print("\nPerda de " + str(round(win, 2)) + "$\n")
            return win
        else:
            print("\nGanho de " + str(round(win, 2)) + "$\n")
            return win
    else:
        return 'error', 0, False
        #print("Por favor, tente novamente")
def definindo_entradas(x,ativos,entrada,acao,time):
    ACTIVES = str(ativos)
    amount = str(entrada)
    action = str(acao)
    duration = str(time)
    if config['digitais'] == 'S':
        print('\n Op. Digitais\n')
        y = entrada_digital(x,ACTIVES,float(amount),action,int(duration))
        #print(f'y{y}')
        return y
    else:
        print('\n Op. Binárias\n')
        y = entrada_turbo(x, float(amount), ACTIVES, action, int(duration))
        #print(f'y{y}')
        return y
def entradas(config, entrada, direcao, timeframe, paridade,x):
    id = definindo_entradas(x, paridade, entrada, direcao, timeframe)

    if id:
        # STOP WIN/STOP LOSS
        banca_att = banca()
        stop_loss = False
        stop_win = False

        if round((banca_att - float(config['banca_inicial'])), 2) <= (abs(float(config['stop_loss'])) * -1.0):
            stop_loss = True

        if round(
            (banca_att - float(config['banca_inicial'])) + (float(entrada) * float(config['payout'])) + float(entrada),
            2) >= abs(float(config['stop_win'])):
            stop_win = True

        while True:
            lucro = id
            if id:
                if lucro < 0:
                    return 'loss'
                if lucro == 0:
                    return 'empate'
                else:
                    return 'win'
                break
def def_tendencia(ativo, tempo):
    par = str(ativo)
    timeframe = int(tempo)
    velas = API.get_candles(par, (int(timeframe) * 60), 14, time.time())
    ultimo = round(velas[0]['close'], 4)
    primeiro = round(velas[-1]['close'], 4)
    diferenca = abs(round(((ultimo - primeiro) / primeiro) * 100, 3))
    tendencia = "CALL" if ultimo < primeiro and diferenca > 0.01 else "PUT" if ultimo > primeiro and diferenca > 0.01 else False
    return tendencia
def filtro_ranking(config):
    user_id = []

    try:
        ranking = API.get_leader_board(
            'Worldwide' if config['filtro_pais'] == 'todos' else config['filtro_pais'].upper(), 1,
            int(config['filtro_top_traders']), 0)

        if int(config['filtro_top_traders']) != 0:
            for n in ranking['result']['positional']:
                id = ranking['result']['positional'][n]['user_id']
                user_id.append(id)
    except:
        pass

    return user_id
config = configuracao()
config['banca_inicial'] = banca()
timeframe = 'PT' + config['timeframe'] + 'M'  # PT5M / PT15M
old = 0
banca_ATUAL = banca()
banca_inicial = float(banca() + float(config['stop_win']))
banca_final = float(banca() - float(config['stop_loss']))
print("Take Profit: ",banca_inicial)
print("Stop Loss: ",banca_final)
print("Banca atual: ",banca_ATUAL)
take_profit = banca_inicial
stop_LOSS = banca_final
tipo_gale = 'simples'
if config['digitais'] == 'S':
    entering = 'digital'
if config['digitais'] == 'N':
    entering = 'turbo'
if config['digitais'] == 'S':
    tipo = 'live-deal-digital-option'
else:
    tipo = 'live-deal-digital-option'
lista = carregar_sinais()
filtro_top_traders = filtro_ranking(config)
premissa = [4, 9, 14, 19, 24, 29, 34, 39, 44, 49, 54, 59]
compra = ['04:3', '09:3', '14:3', '19:3', '24:3', '29:3', '34:3', '39:3', '44:3', '49:3', '54:3', '59:3']
old = 0
#Robôs :
def sinais(valor_entrada, x):
    k = x
    if k - int (1) >= int(config['niveis']):
        print('Iniciando entradas normais.')
        sinais(config['valor_entrada'], 1)
    if k - int (1) >= 1:
        print(f"\nCICLO NÍVEL {k - int (1)}")
    if banca() >= take_profit:
        print("\nBatemos a meta.")
        time.sleep(2)
        sys.exit()
    if banca() <= stop_LOSS:
        print("\nHoje não foi o dia, vamos tentar novamente amanhã.")
        time.sleep(2)
        sys.exit()
    entrada_de =valor_entrada
    while True:
        hora_atual = timestamp_converter(time.time())
        #print(hora_atual)
        for sinal in lista:
            dados = sinal.split(',')
            #1 entrada
            if hora_atual == dados[2]:
                tendendicia = def_tendencia(dados[1], dados[0])
                #config['payout'] = float(payout(dados[1], entering, int(dados[0])) / 100)
                print('\n', dados)
                if tendendicia != dados[3] and config['tendencia'] == 'S':
                    print('\nFora de tendência, aguardando próximo sinal.\nTendência de:', tendendicia)
                    time.sleep(5)
                    print(hora_atual)
                    sinais(entrada_de, k)
                else:
                    print('\nTendência de:', tendendicia)
                    pass
                tempo = f'{dados[0]}'
                time_frame = int(tempo)
                enter = float(entrada_de)
                #print(time_frame)
                entrando= entradas(config, enter, dados[3],time_frame, dados[1],k)
                #resultado, lucro, stop = round(API.check_win_v3(id), 2)
                print(f'{hora_atual}\n')
                print(entrando)
                if banca() >= take_profit:
                    print("\nBatemos a meta.")
                    time.sleep(2)
                    sys.exit()
                if banca() <= stop_LOSS:
                    print("\nHoje não foi o dia, vamos tentar novamente amanhã.")
                    time.sleep(2)
                    sys.exit()
                if entrando == 'empate' and config['digitais'] == 'N':
                    print('Empate')
                    sinais(entrada_de, k)
                if entrando != 'loss':
                    sinais(config['valor_entrada'], 1)
                if entrando == 'loss' and int(config['nivel_p_proximo_sinal']) >= 1:
                    valor_entrada = martingale(tipo_gale, enter)
                    for i in range(int(config['nivel_p_proximo_sinal']) if int(config['nivel_p_proximo_sinal']) > 0 else 1):
                        print('\n   MARTINGALE NIVEL C/ CICLO ' + str(i + 1) + '..\n', end='')
                        resultado = entradas(config, valor_entrada, dados[3],time_frame, dados[1],k)
                        print(' ', resultado, '/', '\n')
                        if banca() >= take_profit:
                            print("Batemos a meta.")
                            time.sleep(2)
                            sys.exit()
                        if banca() <= stop_LOSS:
                            print("Hoje não foi o dia, vamos tentar novamente amanhã.")
                            time.sleep(2)
                            sys.exit()
                        if resultado == 'empate'and config['digitais'] == 'N':
                            print('Empate')
                            sinais(entrada_de, k)
                        if resultado == 'win':
                            print('\n')
                            sinais(config['valor_entrada'], 1)
                            break
                        else:
                            valor_entrada = martingale(tipo_gale, float(valor_entrada))
                            #return valor_entrada
                #print(valor_entrada)
                valor = valor_entrada
                if entrando == 'loss' and config['proximo_sinal'] == 'S':
                    #valor = martingale(tipo_gale, float(valor_entrada))
                    k += 1
                    sinais(valor, k)
                if entrando == 'loss' and config['sorosgale'] == 'S' and config['proximo_sinal'] == 'S':
                    if float(config['valor_entrada']) > 5:

                        lucro_total = 0
                        lucro = 0
                        perca = float(config['valor_entrada'])

                        # Nivel
                        for i in range(int(config['niveis']) if int(config['niveis']) > 0 else 1):

                            # Mao
                            for i2 in range(2):

                                if lucro_total >= perca:
                                    break

                                if banca() >= take_profit:
                                    print("Batemos a meta.")
                                    time.sleep(2)
                                    sys.exit()

                                if banca() <= stop_LOSS:
                                    print("Hoje não foi o dia, vamos tentar novamente amanhã.")
                                    time.sleep(2)
                                    sys.exit()
                                print(x)
                                entrada_de = config['valor_entrada']

                                hora_atual = timestamp_converter(time.time())
                                # print(hora_atual)
                                for sinal in lista:
                                    dados = sinal.split(',')

                                    # print(dados,'\n\n')
                                    # print(hora_atual)

                                    # 1 entrada
                                    if hora_atual == dados[2]:
                                        #config['payout'] = float(payout(dados[1], entering, int(dados[0])) / 100)
                                        print(dados)
                                        print(f'\nEntramos com ${entrada_de}\n\nDireção: {dados[3]}')
                                        tempo = f'{dados[0]}'
                                        time_frame = int(tempo)
                                        enter = float(entrada_de)
                                        # print(time_frame)
                                        print('\n\n   SOROSGALE NIVEL ' + str(i + 1) + ' | MAO ' + str(
                                            i2 + 1) + ' | ', end='')
                                        entrando, resultado = entradas(config, (perca / 2) + lucro, dados[3], time_frame,
                                                                       dados[1],k)
                                        # resultado, lucro, stop = round(API.check_win_v3(id), 2)
                                        print(hora_atual)
                                        print(entrando)

                                        if banca() >= take_profit:
                                            print("Batemos a meta.")
                                            time.sleep(2)
                                            sys.exit()

                                        if banca() <= stop_LOSS:
                                            print("Hoje não foi o dia, vamos tentar novamente amanhã.")
                                            time.sleep(2)
                                            sys.exit()

                                        if resultado == 'win':
                                            lucro_total += lucro
                                        else:
                                            lucro_total = 0
                                            perca += perca / 2
                                            break
                if entrando == 'loss' and config['martingale'] == 'S':
                    valor_entrada = martingale(tipo_gale, float(config['valor_entrada']))
                    for i in range(int(config['niveis']) if int(config['niveis']) > 0 else 1):
                        print('\n   MARTINGALE NIVEL ' + str(i + 1) + '..\n', end='')
                        resultado = entradas(config, valor_entrada, dados[3],time_frame, dados[1],k)
                        print(' ', resultado, '/', '\n')
                        if banca() >= take_profit:
                            print("Batemos a meta.")
                            time.sleep(2)
                            sys.exit()
                        if banca() <= stop_LOSS:
                            print("Hoje não foi o dia, vamos tentar novamente amanhã.")
                            time.sleep(2)
                            sys.exit()
                        if resultado == 'win':
                            print('\n')
                            sinais(config['valor_entrada'], 1)
                            break
                        else:
                            valor_entrada = martingale(tipo_gale, float(config['valor_entrada']))
def bot_mhi(valor_entrada,x):
    global direcao
    k = x
    enter = float(valor_entrada)
    while True:
        horario = time_mhi(time.time())

        while str(horario[17:20]) == str(35):
            horario = time_mhi(time.time())
            candle = pd.DataFrame(columns=['data' , 'candle'])
            candles = API.get_candles(config['paridade'] , 60 , 3 , time.time())
            print(horario)

            if banca() >= take_profit:
                print("Batemos a meta.")
                time.sleep(2)
                sys.exit()

            if banca() <= stop_LOSS:
                print("Hoje não foi o dia, vamos tentar novamente amanhã.")
                time.sleep(2)
                sys.exit()

            # TRANSFORMAR AS INFORMAÇÕES ADQUIRIDAS
            if str(horario[14:16]) in str(premissa).zfill(2):
                for cand in candles:
                    vela = None
                    if cand['open'] > cand['close']:
                        vela = 1  # vermelho
                    elif cand['open'] == cand['close']:
                        vela = 0  # cinza
                    elif cand['open'] < cand['close']:
                        vela = 2  # verde

                    if any(str(i) == str(time_mhi(cand['from'])) for i in candle['data']):
                        pass
                    else:
                        candle = candle.append({'data': time_mhi(cand['from']) ,
                                                'candle': vela} , ignore_index=True)
                print('pegou os candles')

                # DECIDIR PRA QUE LADO OPERAR - COMPRA OU VENDA
                try:
                    maioria = candle.groupby(by='candle')['candle'].count().sort_values(ascending=False)
                    maioria = maioria.index[0]
                    if maioria == 1:
                        direcao = 'call'
                    elif maioria == 2:
                        direcao = 'put'
                    elif maioria != 1 or maioria != 2:
                        print('Sem direção exata!')
                        bot_mhi(enter, k)
                    print('decidiu a maioria')
                except:
                    pass
                # OPERAR
                if str(horario[14:18]) in str(compra).zfill(2):
                    tendendicia = def_tendencia(config['paridade'], config['timeframe'])
                    if tendendicia != direcao and config['tendencia'] == 'S':
                        print('\nFora de tendência, aguardando próximo sinal.\nTendência de:', tendendicia)
                        time.sleep(5)
                        bot_mhi(enter, k)
                    else:
                        print('\nTendência de:', tendendicia)
                        pass
                    entrando= entradas(config, enter, direcao,config['timeframe'], config['paridade'],k)
                    print(f'{horario}\n')
                    print(entrando)
                    if banca() >= take_profit:
                        print("\nBatemos a meta.")
                        time.sleep(2)
                        sys.exit()
                    if banca() <= stop_LOSS:
                        print("\nHoje não foi o dia, vamos tentar novamente amanhã.")
                        time.sleep(2)
                        sys.exit()
                    if entrando == 'empate'and config['digitais'] == 'N':
                        print('Empate')
                        bot_mhi(enter, k)
                    if entrando != 'loss':
                        bot_mhi(config['valor_entrada'], 1)
                    if entrando == 'loss' and int(config['nivel_p_proximo_sinal']) >= 1:
                        valor_entrada = martingale(tipo_gale, enter)
                        for i in range(
                            int(config['nivel_p_proximo_sinal']) if int(config['nivel_p_proximo_sinal']) > 0 else 1):
                            print('\n   MARTINGALE NIVEL C/ CICLO ' + str(i + 1) + '..\n', end='')
                            resultado = entradas(config, valor_entrada, direcao,config['timeframe'], config['paridade'],k)
                            print(' ', resultado, '/', '\n')
                            if banca() >= take_profit:
                                print("Batemos a meta.")
                                time.sleep(2)
                                sys.exit()
                            if banca() <= stop_LOSS:
                                print("Hoje não foi o dia, vamos tentar novamente amanhã.")
                                time.sleep(2)
                                sys.exit()
                            if resultado == 'empate' and config['digitais'] == 'N':
                                print('Empate')
                                bot_mhi(enter, k)
                            if resultado == 'win':
                                print('\n')
                                bot_mhi(config['valor_entrada'], 1)
                                break
                            else:
                                valor_entrada = martingale(tipo_gale, float(valor_entrada))
                                # return valor_entrada
                    print(valor_entrada)
                    valor = valor_entrada
                    if entrando == 'loss' and config['proximo_sinal'] == 'S':
                        # valor = martingale(tipo_gale, float(valor_entrada))
                        k += 1
                        bot_mhi(valor, k)
                    '''if entrando == 'loss' and config['sorosgale'] == 'S' and config['proximo_sinal'] == 'S':
                        if float(config['valor_entrada']) > 5:

                            lucro_total = 0
                            lucro = 0
                            perca = float(config['valor_entrada'])

                            # Nivel
                            for i in range(int(config['niveis']) if int(config['niveis']) > 0 else 1):

                                # Mao
                                for i2 in range(2):

                                    if lucro_total >= perca:
                                        break

                                    if banca() >= take_profit:
                                        print("Batemos a meta.")
                                        time.sleep(2)
                                        sys.exit()

                                    if banca() <= stop_LOSS:
                                        print("Hoje não foi o dia, vamos tentar novamente amanhã.")
                                        time.sleep(2)
                                        sys.exit()
                                    print(x)
                                    entrada_de = config['valor_entrada']

                                    hora_atual = timestamp_converter(time.time())
                                    # print(hora_atual)
                                    for sinal in lista:
                                        dados = sinal.split(',')

                                        # print(dados,'\n\n')
                                        # print(hora_atual)

                                        # 1 entrada
                                        if hora_atual == dados[2]:
                                            # config['payout'] = float(payout(dados[1], entering, int(dados[0])) / 100)
                                            print(dados)
                                            print(f'\nEntramos com ${entrada_de}\n\nDireção: {dados[3]}')
                                            tempo = f'{dados[0]}'
                                            time_frame = int(tempo)
                                            enter = float(entrada_de)
                                            # print(time_frame)
                                            print('\n\n   SOROSGALE NIVEL ' + str(i + 1) + ' | MAO ' + str(
                                                i2 + 1) + ' | ', end='')
                                            entrando, resultado = entradas(config, (perca / 2) + lucro, dados[3],
                                                                           time_frame,
                                                                           dados[1], k)
                                            # resultado, lucro, stop = round(API.check_win_v3(id), 2)
                                            print(hora_atual)
                                            print(entrando)

                                            if banca() >= take_profit:
                                                print("Batemos a meta.")
                                                time.sleep(2)
                                                sys.exit()

                                            if banca() <= stop_LOSS:
                                                print("Hoje não foi o dia, vamos tentar novamente amanhã.")
                                                time.sleep(2)
                                                sys.exit()

                                            if resultado == 'win':
                                                lucro_total += lucro
                                            else:
                                                lucro_total = 0
                                                perca += perca / 2
                                                break'''
                    if entrando == 'loss' and config['martingale'] == 'S':
                        valor_entrada = martingale(tipo_gale, float(config['valor_entrada']))
                        for i in range(int(config['niveis']) if int(config['niveis']) > 0 else 1):
                            print('\n   MARTINGALE NIVEL ' + str(i + 1) + '..\n', end='')
                            resultado = entradas(config, valor_entrada, direcao,config['timeframe'], config['paridade'],k)
                            print(' ', resultado, '/', '\n')
                            if banca() >= take_profit:
                                print("Batemos a meta.")
                                time.sleep(2)
                                sys.exit()
                            if banca() <= stop_LOSS:
                                print("Hoje não foi o dia, vamos tentar novamente amanhã.")
                                time.sleep(2)
                                sys.exit()
                            if resultado == 'win':
                                print('\n')
                                bot_mhi(config['valor_entrada'], 1)
                                break
                            else:
                                valor_entrada = martingale(tipo_gale, float(valor_entrada))

if __name__ == '__main__':
    if config['sinais'] == "S":
        print('\n######## Carregando Sinais ########')
        sinais(config['valor_entrada'], 1)
    if config['mhi'] == "S":
            print('\n######## Ativando MHI ########')
            bot_mhi(config['valor_entrada'], 1)
    if config['copy'] == "S":
        print('Ainda em desenvolvimento')
        #print('\n######## Ativando Copy Trade ########')
        #copy_trade(config['valor_entrada'], 1)
